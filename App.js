import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';

import MainApp from './src';
const App = () => {
  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <MainApp />
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
