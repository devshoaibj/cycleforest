import React, {useEffect, useRef, useState} from 'react';
import {
  Alert,
  SafeAreaView,
  StyleSheet,
  Image,
  ActivityIndicator,
  ScrollView,
  Dimensions,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';
import * as constraints from './constraints';

import _ from 'lodash';

import Container from './Container';
import Label from './Label';
import CustomForm from './CustomForm';
import Button from './Button';
import Input from './Input';
import * as Data from './Data';

import Modal from 'react-native-modal';
const {width, height} = Dimensions.get('window');
const Stack = createStackNavigator();
const App = () => {
  useEffect(() => {}, []);
  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="CycleDetails" component={CycleDetails} />
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    </>
  );
};
function Home({navigation}) {
  const [cycles, setCycles] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selected, setSelected] = useState(1);
  const [selectedCat, setSelectedCat] = useState('Bikes for Kids');
  useEffect(() => {
    setCycles(Data.cycles);
    setCategories(Data.cats);
  }, []);
  return (
    <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      style={{
        backgroundColor: constraints.colors.white,
        flex: 1,
        height: height,
      }}
      alwaysBounceVertical={false}
      alwaysBounceHorizontal={false}
      bounces={false}
      bouncesZoom={false}>
      <Container padding={[constraints.sizes.base * 2, 0]}>
        <Container padding={[0, constraints.sizes.base * 2]}>
          <Container center row>
            <Label gray bold title>
              Hi there, Welcom to
            </Label>
            <Label primary bold h2 style={{marginLeft: 6}}>
              Cycle Forest
            </Label>
          </Container>
          <Container row style={{marginBottom: constraints.sizes.base * 1.5}}>
            <Label secondary bold size={35}>
              Have a nice day!
            </Label>
          </Container>
        </Container>
        <Container row padding={[0, constraints.sizes.base]}>
          <FlatList
            horizontal={true}
            data={categories}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item, index}) => (
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => {
                  setSelected(item.id);
                  setSelectedCat(item.title);
                }}>
                <Container
                  flex={false}
                  style={{
                    width: width / 4.35,
                    height: width / 4,
                  }}>
                  <Container
                    flex={false}
                    style={[
                      {
                        width: width / 5,
                        height: width / 5,
                        borderRadius: 15,
                        alignContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        textAlignVertical: 'center',
                      },
                      selected === item.id
                        ? {
                            backgroundColor: constraints.colors.primary,
                          }
                        : {
                            backgroundColor: constraints.colors.gray6,
                          },
                    ]}>
                    <Container center middle>
                      <Image
                        source={selected === item.id ? item.iconw : item.icon}
                        style={{
                          width: 35,
                          height: 35,
                        }}
                      />
                    </Container>
                  </Container>

                  {/* <Label
                    size={12}
                    bold
                    center
                    style={[
                      {marginTop: 6},
                      selected === item.id
                        ? {color: constraints.colors.secondary}
                        : {color: constraints.colors.gray},
                    ]}>
                    {item.title}
                  </Label> */}
                </Container>
              </TouchableOpacity>
            )}
          />
        </Container>
        <Container
          row
          center
          padding={[constraints.sizes.base / 2, constraints.sizes.base]}>
          <Label h2 secondary bold>
            {selectedCat}
          </Label>
        </Container>
        <Container row padding={[0, constraints.sizes.base * 2]}>
          <FlatList
            horizontal={true}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            data={_.filter(cycles, {catid: selected})}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item}) => (
              <Container
                key={'prod-' + item.id}
                card
                flex={false}
                style={[styles.cycleBox]}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() =>
                    navigation.navigate('CycleDetails', {
                      cycleItem: item,
                    })
                  }>
                  <Container column>
                    <Container flex={false}>
                      <Image source={item.image} style={styles.cycleImg} />
                    </Container>
                    <Container
                      padding={[
                        constraints.sizes.base / 2,
                        constraints.sizes.base / 2,
                      ]}>
                      <Container
                        flex={false}
                        style={{width: '100%', paddingRight: 5}}>
                        <Label size={16} gray bold>
                          {item.title}
                        </Label>
                      </Container>
                      <Container flex={false}>
                        <Label size={20} primary bold>
                          ${item.price}
                        </Label>
                      </Container>
                    </Container>
                  </Container>
                </TouchableOpacity>
              </Container>
            )}
          />
        </Container>
        <Container
          row
          center
          padding={[constraints.sizes.base / 2, constraints.sizes.base]}
          style={{marginTop: 8}}>
          <Label h1 secondary bold>
            Top Sellers -
          </Label>
        </Container>
        <Container row padding={[0, constraints.sizes.base * 2]}>
          <FlatList
            horizontal={true}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            data={Data.topSaller}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item}) => (
              <Container
                key={'prod-' + item.id}
                card
                flex={false}
                style={[styles.cycleBox2]}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() =>
                    navigation.navigate('CycleDetails', {
                      cycleItem: item,
                    })
                  }>
                  <Container column>
                    <Container flex={false}>
                      <Image source={item.image} style={styles.cycleImg2} />
                    </Container>
                    <Container
                      padding={[
                        constraints.sizes.base / 2,
                        constraints.sizes.base / 2,
                      ]}>
                      <Container
                        flex={false}
                        style={{width: '100%', paddingRight: 5}}>
                        <Label size={16} gray bold>
                          {item.title}
                        </Label>
                      </Container>
                      <Container flex={false}>
                        <Label size={20} primary bold>
                          ${item.price}
                        </Label>
                      </Container>
                    </Container>
                  </Container>
                </TouchableOpacity>
              </Container>
            )}
          />
        </Container>
        <Container
          row
          center
          padding={[constraints.sizes.base / 2, constraints.sizes.base]}
          style={{marginTop: 8}}>
          <Label h1 secondary bold>
            Featured -
          </Label>
        </Container>
        <Container row padding={[0, constraints.sizes.base * 2]}>
          <FlatList
            horizontal={true}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            data={Data.features}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item}) => (
              <Container
                key={'prod-' + item.id}
                card
                flex={false}
                style={[styles.cycleBox2]}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() =>
                    navigation.navigate('CycleDetails', {
                      cycleItem: item,
                    })
                  }>
                  <Container column>
                    <Container flex={false}>
                      <Image source={item.image} style={styles.cycleImg2} />
                    </Container>
                    <Container
                      padding={[
                        constraints.sizes.base / 2,
                        constraints.sizes.base / 2,
                      ]}>
                      <Container
                        flex={false}
                        style={{width: '100%', paddingRight: 5}}>
                        <Label size={16} gray bold>
                          {item.title}
                        </Label>
                      </Container>
                      <Container flex={false}>
                        <Label size={20} primary bold>
                          ${item.price}
                        </Label>
                      </Container>
                    </Container>
                  </Container>
                </TouchableOpacity>
              </Container>
            )}
          />
        </Container>
      </Container>
    </ScrollView>
  );
}
function CycleDetails({route, navigation}) {
  const inputRefs = [useRef(), useRef(), useRef(), useRef(), useRef()];
  const {cycleItem} = route.params;
  const [alertData, setAlert] = useState({isAlert: false, message: ''});
  const [isShowModel, setModal] = useState(false);
  const [orderCustomForm, setorderCustomForm] = useState({
    Email: null,
    FirstName: null,
    LastName: null,
    PhoneNumber: null,
    Address: null,
  });
  const [mainLoader, setMainLoader] = useState(false);
  sendRequestAPI = async () => {
    try {
      setMainLoader(true);
      const data = await fetch('https://rnapps-backend.herokuapp.com/leads', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          firstname: orderCustomForm.FirstName,
          lastname: orderCustomForm.LastName,
          email: orderCustomForm.Email,
          phonenumber: orderCustomForm.PhoneNumber,
          address: orderCustomForm.Address,
          appname: 'Cycle Forest',
        }),
      });
      if (data.status === 200) {
        await setMainLoader(false);
        await setorderCustomForm({
          Email: null,
          FirstName: null,
          LastName: null,
          PhoneNumber: null,
          Address: null,
        });
        await setModal(false);
      }
    } catch (error) {}
    Alert.alert(
      'COMPELTED ORDER',
      'Thank you for choosing us, You have ordered to us. Our delivery team will contact you soon. Please prepare the exact amount of $' +
        cycleItem.price +
        ' and pay them.',
      [
        {
          text: 'Okay',
          onPress: () =>  navigation.navigate('Home')
        },
      ],
    );
   
  };
  sendRequest = async () => {
    console.log(orderCustomForm);
    if (
      orderCustomForm.FirstName === null ||
      orderCustomForm.FirstName === ''
    ) {
      setAlert({isAlert: true, message: 'Please enter First name field.'});
    } else if (
      orderCustomForm.LastName === null ||
      orderCustomForm.LastName === ''
    ) {
      setAlert({isAlert: true, message: 'Please enter Last name field.'});
    } else if (orderCustomForm.Email === null || orderCustomForm.Email === '') {
      setAlert({isAlert: true, message: 'Please enter Email Address field.'});
    } else if (
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        orderCustomForm.Email,
      ) === false
    ) {
      setAlert({isAlert: true, message: 'Invliad Email Address'});
    } else if (
      orderCustomForm.Address === null ||
      orderCustomForm.Address === ''
    ) {
      setAlert({isAlert: true, message: 'Please enter Address field.'});
    } else {
      console.log('Starting');
      sendRequestAPI();
    }
    setTimeout(() => {
      setAlert({isAlert: false, message: ''});
    }, 3000);
  };
  loaderComp = () => {
    return (
      <SafeAreaView style={styles.loaderContrainer}>
        <Container center middle column flex={1}>
          <Container center middle>
            <ActivityIndicator
              size="large"
              color={constraints.colors.white}
              animating={true}
            />
          </Container>
        </Container>
      </SafeAreaView>
    );
  };
  orderForm = () => {
    return (
      <>
        {mainLoader && loaderComp()}
        <CustomForm>
          <>
            <Container
              column
              padding={[constraints.sizes.base / 3, constraints.sizes.base]}>
              <Container
                row
                padding={[constraints.sizes.base / 2, 0]}
                style={{width: width / 1.1}}>
                <Label header gray bold left>
                  Please fill your delivery address details. you will pay to our
                  cargo becuase our service is cash on delivery.
                </Label>
              </Container>
              {alertData.isAlert === true && (
                <Container
                  row
                  padding={[constraints.sizes.base / 2, 0]}
                  center
                  middle>
                  <Label header color="red" bold center>
                    {alertData.message}
                  </Label>
                </Container>
              )}
              <Input
                type={1}
                label={'First Name'}
                returnKeyType={'next'}
                blurOnSubmit={false}
                forwardRef={inputRefs[0]}
                onSubmitEditing={() => inputRefs[1].current.focus()}
                value={orderCustomForm.FirstName}
                onChangeText={(FirstName) =>
                  setorderCustomForm({...orderCustomForm, FirstName})
                }
              />
              <Input
                type={1}
                label={'Last Name'}
                returnKeyType={'next'}
                blurOnSubmit={false}
                forwardRef={inputRefs[1]}
                onSubmitEditing={() => inputRefs[2].current.focus()}
                value={orderCustomForm.LastName}
                onChangeText={(LastName) =>
                  setorderCustomForm({...orderCustomForm, LastName})
                }
              />
              <Input
                email
                type={1}
                label={'Email'}
                returnKeyType={'next'}
                blurOnSubmit={false}
                forwardRef={inputRefs[2]}
                onSubmitEditing={() => inputRefs[3].current.focus()}
                value={orderCustomForm.Email}
                onChangeText={(Email) =>
                  setorderCustomForm({...orderCustomForm, Email})
                }
              />
              <Input
                number
                type={1}
                label={'Phone Number'}
                returnKeyType={'next'}
                blurOnSubmit={false}
                forwardRef={inputRefs[3]}
                onSubmitEditing={() => inputRefs[4].current.focus()}
                value={orderCustomForm.PhoneNumber}
                onChangeText={(PhoneNumber) =>
                  setorderCustomForm({...orderCustomForm, PhoneNumber})
                }
              />

              <Input
                type={2}
                label={'Address'}
                returnKeyType={'done'}
                forwardRef={inputRefs[4]}
                value={orderCustomForm.Address}
                onChangeText={(Address) =>
                  setorderCustomForm({...orderCustomForm, Address})
                }
                style={{
                  height: constraints.sizes.base * 4,
                }}
              />
              <Button
                color={constraints.colors.primary}
                onPress={() => sendRequest()}>
                <Label white header bold center>
                  SUBMIT REQUEST
                </Label>
              </Button>
            </Container>
          </>
        </CustomForm>
      </>
    );
  };
  return (
    <>
      <ScrollView
        style={{
          backgroundColor: constraints.colors.white,
          height: height,
        }}>
        <Container column>
          <Image
            source={cycleItem.image}
            style={{width: width, height: height / 3.4}}
          />
          <Container flex={false} style={styles.backPanel}>
            <TouchableOpacity
              activeOpacity={0.3}
              onPress={() => navigation.goBack()}>
              <Image
                source={require('../assets/icons/back.png')}
                style={{width: 30, height: 30, resizeMode: 'contain'}}
              />
            </TouchableOpacity>
          </Container>
          <Container
            padding={[constraints.sizes.base, constraints.sizes.base * 1.5]}>
            <Container row center>
              <Label size={25} secondary bold>
                {cycleItem.title}
              </Label>
            </Container>
            <Container
              row
              center
              style={{
                borderColor: constraints.colors.gray5,
                borderBottomWidth: StyleSheet.hairlineWidth,
              }}>
              <Label size={30} primary bold>
                ${cycleItem.price}
              </Label>
            </Container>
            <Container
              row
              center
              style={{marginTop: constraints.sizes.base / 2}}>
              <Label h3 secondary bold>
                Specifications
              </Label>
            </Container>
            <Container
              row
              center
              style={{marginTop: constraints.sizes.base / 2}}>
              <Label
                header
                gray
                height={23}
                style={{textAlign: 'left', alignSelf: 'stretch'}}>
                {cycleItem.desc}
              </Label>
            </Container>
            <Container
              row
              center
              middle
              style={{marginTop: constraints.sizes.base}}>
              <Button
                color={constraints.colors.primary}
                style={{
                  height: 50,
                  width: width - constraints.sizes.base * 2,
                }}
                onPress={() => setModal(true)}>
                <Label h3 white bold center>
                  PROCEED WTIH CHECKOUT
                </Label>
              </Button>
            </Container>
          </Container>
        </Container>
      </ScrollView>
      <Modal
        testID={'modal'}
        isVisible={isShowModel}
        onBackdropPress={() => setModal(false)}
        swipeDirection={'down'}
        style={{
          margin: 0,
          justifyContent: 'flex-end',
          zIndex: 10,
        }}
        backdropOpacity={0.2}>
        <Container
          flex={false}
          style={{
            backgroundColor: constraints.colors.white,
            borderRadius: 5,
            width: width,
            height: height / 1.5,
          }}>
          {orderForm()}
        </Container>
      </Modal>
    </>
  );
}
const styles = StyleSheet.create({
  headerIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 8,
  },
  cycleBox: {
    backgroundColor: constraints.colors.gray6,
    marginRight: 15,
    borderRadius: 15,
    width: width / 1.7 - constraints.sizes.base * 3,
    
  },
  cycleImg: {
    width: width / 1.7 - constraints.sizes.base * 3,
    height: width / 3.8,
    resizeMode: 'contain',
  },
  cycleBox2: {
    backgroundColor: constraints.colors.gray6,
    marginRight: 15,
    borderRadius: 15,
    width: width / 1.3 - constraints.sizes.base * 3,
   
  },
  cycleImg2: {
    width: width / 1.3 - constraints.sizes.base * 3,
    height: width / 2.8,
    resizeMode: 'contain',
  },
  backPanel: {
    position: 'absolute',
    top: 15,
    left: 10,
    zIndex: 1000,
    padding: 15,
  },
  loaderContrainer: {
    position: 'absolute',
    zIndex: 1,
    flex: 1,
    height: height,
    width: width,
    backgroundColor: '#00000050',
  },
  alertContainer: {
    backgroundColor: constraints.colors.primary,
    borderRadius: 50,
    padding: constraints.sizes.base,
    top: constraints.sizes.base * 2,
    zIndex: 1000,
    position: 'absolute',
    marginVertical: constraints.sizes.base,
  },
});

export default App;
