import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import * as constraints from './constraints';

class Button extends Component {
  render() {
    const {
      style,
      opacity,
      color,
      startColor,
      endColor,
      end,
      start,
      locations,
      shadow,
      children,
      ...props
    } = this.props;

    const buttonStyles = [
      styles.button,
      shadow && styles.shadow,
      color && styles[color],
      color && !styles[color] && {backgroundColor: color},
      style,
    ];

    return (
      <TouchableOpacity
        style={buttonStyles}
        activeOpacity={opacity || 0.8}
        {...props}>
        {children}
      </TouchableOpacity>
    );
  }
}

Button.defaultProps = {
  startColor: constraints.colors.primary,
  endColor: constraints.colors.secondary,
  start: {x: 0, y: 0},
  end: {x: 1, y: 1},
  locations: [0.1, 0.9],
  opacity: 0.8,
  color: constraints.colors.white,
};

export default Button;

const styles = StyleSheet.create({
  button: {
    borderRadius: constraints.sizes.radius,
    height: constraints.sizes.base * 3,
    justifyContent: 'center',
    marginVertical: constraints.sizes.padding / 2.5,
  },
  shadow: {
    shadowColor: constraints.colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },
  accent: {backgroundColor: constraints.colors.accent},
  primary: {backgroundColor: constraints.colors.primary},
  secondary: {backgroundColor: constraints.colors.secondary},
  tertiary: {backgroundColor: constraints.colors.tertiary},
  black: {backgroundColor: constraints.colors.black},
  white: {backgroundColor: constraints.colors.white},
  gray: {backgroundColor: constraints.colors.gray},
  gray2: {backgroundColor: constraints.colors.gray2},
  gray3: {backgroundColor: constraints.colors.gray3},
  gray4: {backgroundColor: constraints.colors.gray4},
});
