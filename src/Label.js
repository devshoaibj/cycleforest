import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import * as constraints from './constraints';
export default class Label extends Component {
  render() {
    const {
      h1,
      h2,
      h3,
      title,
      size,
      bold,
      semibold,
      medium,
      center,
      right,
      spacing,
      height,
      color,
      accent,
      primary,
      secondary,
      tertiary,
      black,
      white,
      gray,
      gray2,
      gray3,
      gray5,
      style,
      children,
      ...props
    } = this.props;
    const textStyles = [
      styles.text,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      title && styles.title,
      size && {fontSize: size},
      height && {lineHeight: height},
      spacing && {letterSpacing: spacing},
      bold && styles.bold,
      semibold && styles.semibold,
      medium && styles.medium,
      center && styles.center,
      right && styles.right,
      color && styles[color],
      color && !styles[color] && {color},
      accent && styles.accent,
      primary && styles.primary,
      secondary && styles.secondary,
      tertiary && styles.tertiary,
      black && styles.black,
      white && styles.white,
      gray && styles.gray,
      gray2 && styles.gray2,
      gray3 && styles.gray3,
      gray5 && styles.gray5,
      style,
    ];
    return (
      <Text style={textStyles} {...props}>
        {children}
      </Text>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: constraints.sizes.font,
    color: constraints.colors.black,
  },
  regular: {
    fontWeight: 'normal',
  },
  bold: {
    fontWeight: 'bold',
  },
  semibold: {
    fontWeight: '500',
  },
  medium: {
    fontWeight: '500',
  },
  center: {textAlign: 'center'},
  right: {textAlign: 'right'},
  accent: {color: constraints.colors.accent},
  primary: {color: constraints.colors.primary},
  secondary: {color: constraints.colors.secondary},
  tertiary: {color: constraints.colors.tertiary},
  black: {color: constraints.colors.black},
  white: {color: constraints.colors.white},
  white2: {color: constraints.colors.white2},
  gray: {color: constraints.colors.gray},
  gray2: {color: constraints.colors.gray2},
  gray3: {color: constraints.colors.gray3},
  gray5: {color: constraints.colors.gray5},
  h1: constraints.fonts.h1,
  h2: constraints.fonts.h2,
  h3: constraints.fonts.h3,
  title: constraints.fonts.title,
});
