import React, {Component} from 'react';
import {StyleSheet, TextInput} from 'react-native';
import Label from './Label';
import Container from './Container';
import * as constraints from './constraints';

export default class Input extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      type,
      email,
      phone,
      number,
      secure,
      error,
      border,
      style,
      label,
      ...props
    } = this.props;
    const inputType = email
      ? 'email-address'
      : number
      ? 'numeric'
      : phone
      ? 'phone-pad'
      : 'default';
    if (type === 1) {
      return (
        <Container flex={false} margin={[constraints.sizes.base / 2, 0]}>
          <Container flex={false}>
            {label ? (
              <Label
                header
                secondary
                spacing={0.6}
                style={{
                  paddingLeft: constraints.sizes.base / 3,
                }}>
                {label}
              </Label>
            ) : null}
          </Container>
          <TextInput
            style={styles.input1}
            autoComplete="off"
            autoCapitalize="sentences"
            autoCorrect={false}
            keyboardType={inputType}
            ref={this.props.forwardRef}
            onSubmitEditing={this.props.onSubmitEditing}
            {...props}
          />
        </Container>
      );
    } else if (type === 2) {
      return (
        <Container flex={false} margin={[constraints.sizes.base / 2, 0]}>
          <Container flex={false}>
            {label ? (
              <Label
                header
                secondary
                spacing={0.6}
                style={{
                  paddingLeft: constraints.sizes.base / 3,
                }}>
                {label}
              </Label>
            ) : null}
          </Container>
          <TextInput
            style={styles.input2}
            autoComplete="off"
            autoCapitalize="sentences"
            multiline={true}
            autoCorrect={false}
            keyboardType={inputType}
            ref={this.props.forwardRef}
            onSubmitEditing={this.props.onSubmitEditing}
            {...props}
          />
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  input1: {
    backgroundColor: 'transparent',
    fontWeight: '300',
    color: constraints.colors.secondary,
    height: constraints.sizes.base * 2.5,
    fontSize: 17,
    borderRadius: 5,
    paddingLeft: constraints.sizes.base / 3,
    paddingBottom: constraints.sizes.base / 3,
    borderColor: constraints.colors.gray4,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: constraints.colors.secondary,
    borderBottomWidth: StyleSheet.hairlineWidth * 1.3,
  },
  input2: {
    backgroundColor: 'transparent',
    fontWeight: '300',
    color: constraints.colors.secondary,
    height: constraints.sizes.base * 3.5,
    fontSize: 17,
    borderRadius: 5,
    paddingHorizontal: constraints.sizes.base / 2,
    paddingVertical: constraints.sizes.base / 3,
    borderColor: constraints.colors.secondary,
    borderBottomWidth: StyleSheet.hairlineWidth,
    textAlignVertical: 'top',
  },
});
