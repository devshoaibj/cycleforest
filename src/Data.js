const cycles = [
  {
    id: 1,
    image: require('../assets/data/kids/Ghost_Kato_Essential_24.jpg'),
    title: `Ghost Kato 24'' MTB`,
    desc: `Frame	KATO Kid AL, ALUMINUM
    Fork	Suntour XCT-JR, 50mm
    Stem	Ghost AS-GH2, 31.8mm
    Handlebars	Ghost Low Rizer Kid, 31.8x580mm, 15mm rise
    Handles / handlebar tape	Velo VLG 213
    Gear lever	Shimano Tourney
    Brake lever	Tektro HD-M282
    Rear derailleur	Shimano Tourney TX RD-TX800, 8-S
    Derailleur	Shimano Tourney FD-TY500-TS6
    Bottom bracket	Tuna
    Crankset	Samox 38/24
    cassette	Shimano CS-HG-200-8, 12-32t
    Chain	KMC Z51
    Derailleur hanger	EZ2036
    Brakes	Tektro HD M-282
    Brake discs	Tektro, 160mm
    saddle	GHOST VL 5062
    Seat post	GHOST SP DC1 31.6mm
    Seat clamp	GHOST XC 68 34.9mm
    Impeller size	24 ''
    Front / rear wheel	Rodi Viper Wheelset 507x21mm QR / Rodi Viper Wheelset 507x21mm QR
    Front / rear tires	Mitas Ocelot 24x2.10 / Mitas Ocelot 24x2.10
    Pedals	VP-225`,
    price: '450',
    catid: 1,
  },
  {
    id: 2,
    image: require('../assets/data/kids/Ghost_Powerkid_AL_12.jpg'),
    title: `Ghost Powerkid AL 12''`,
    desc: `Frame	Powerkid AL
    Fork	Ghost AL Rigid 12
    Porch	Ghost Powerkid
    Handlebars	Ghost Powerkid
    Crankset	Ghost Powerkid
    Derailleur hanger	EZ2036
    Brakes	Front: V-brake, Rear: back pedal brake
    Saddle	Velo
    Seatpost	Leadtec
    Wheel size	12 "
    Impellers	AL 12
    Front hub	Joytech / KT
    HR-hub	Joytech / KT
    Tires	Kenda 12
    Mass	7.30 kg`,
    price: '400',
    catid: 1,
  },
  {
    id: 3,
    image: require('../assets/data/kids/Ghost_Powerkid_AL_16.jpg'),
    title: `Ghost Powerkid AL 16''`,
    desc: `Frame	Powerkid AL
    Fork	Ghost AL Rigid 16
    Porch	Ghost Powerkid
    Handlebars	Ghost Powerkid
    Crankset	Ghost Powerkid
    Derailleur hanger	EZ2036
    Brakes	Front: V-brake, Rear: back pedal brake
    Saddle	Velo
    Seatpost	Leadtec
    Wheel size	16 "
    Impellers	AL 16
    Front hub	Joytech / KT
    HR-hub	Joytech / KT
    Tires	Kenda 16
    Mass	9.60 kg`,
    price: '359',
    catid: 1,
  },
  {
    id: 4,
    image: require('../assets/data/kids/Ghost_Powerkiddy_AL_12.jpg'),
    title: `Ghost Powerkiddy AL 12''`,
    desc: `Frame	Powerkiddy AL
    Fork	Ghost AL Rigid
    Porch	Ghost Powerkiddy
    Handlebars	Ghost Powerkiddy
    Derailleur hanger	EZ2036
    Seatpost	Leadtec
    Wheel size	12 "
    Impellers	AL 12
    Front hub	Joytech / KT
    HR-hub	Joytech / KT
    Tires	Kenda 12
    Mass	4.80 kg`,
    price: '399',
    catid: 1,
  },
  {
    id: 5,
    image: require('../assets/data/kids/Specialized_Hotrock_20.jpg'),
    title: `Specialized Hotrock 20'' MTB`,
    desc: `Frame	Specialized A1 Premium Aluminum, single-butted, unisex frame, replaceable derailleur hanger
    Fork	SR Suntour XCT-JR, coil spring w / preload adjust, 1-1 / 8 "steerer, quick-release, V-brake & post-mount disc compatible, 40mm of travel
    Stem	20-degree, 25.4mm clamp
    Handlebars	Double-butted alloy, 5-degree backsweep, 25.4mm
    Handles / handlebar tape	Specialized , slip-on
    Gear lever	Shimano Tourney, grip shift, 7-speed
    Rear derailleur	Shimano Tourney TX, 7-speed
    Bottom bracket	BSA, square taper, E-type
    Crankset	Forged alloy, square taper, 127mm w / chainguard
    cassette	SunRace, 7-speed, 11-34t
    Chain	KMC HV500
    Brakes	V-brake w / tool-less reach adjust brake lever
    saddle	Specialized Kids' Henge
    Seat post	Alloy, single-bolt, 27.2mm
    Impeller size	20 "
    Rims	20 "alloy, 16mm depth, 21mm internal width
    Front hub / rear hub	Alloy, loose ball bearings, bolt type, 32h / Shimano Tourney, 7-speed freehub, 32h
    Spokes	Stainless, 14g
    Front / rear tires	Renegade, 26 TPI, wire bead, 20x2.1 "
    Hoses / sealing milk	Schrader valve
    Pedals	Nylon platform w / reflectors`,
    price: '599',
    catid: 1,
  },
  {
    id: 6,
    image: require('../assets/data/kids/Specialized_Hotrock_24.jpg'),
    title: `Specialized Hotrock 24'' MTB`,
    desc: `RIMS	24 ", single wall aluminum, 32h
    FRONT TIRE	Specialized ™ Renegade, 24x2.1 ", 26 TPI, wire bead
    REAR TIRE	Specialized ™ Renegade, 24x2.1 ", 26 TPI, wire bead
    SPOKES	Stainless, 14g
    TUBES	Schrader valve
    FRONT HUB	Alloy, loose ball bearings, bolt type, 32h
    REAR HUB	Alloy, 8-speed freehub, loose ball bearings, bolt type, 32h
    PEDALS	Nylon platform w / reflectors
    BOTTOM BRACKET	BSA, square taper, E-type
    SWITCHER	Shimano Tourney TX, 8-speed
    CHAINRINGS	34T
    CASSETTE	Sunrace, 8-speed, 11-34t
    CRANK	Forged alloy, square taper, 152mm w / chainguard
    ROCKER	Shimano Tourney, Revo, 8-speed
    CHAIN	KMC HV500
    SEAT POST	Alloy, single bolt, 15mm offset, 27.2mm
    BAR	60mm, 5-degree backsweep, 25.4mm
    GRIPS	Specialized ™, slip-on
    SADDLE	Specialized ™ Kids
    STEM	20-degree rise, 25.4mm
    REAR BRAKE	V-brake w / tool-less reach adjust brake lever
    FRONT BRAKE	V-brake, alloy, linear pull, tool-less reach adjust brake lever
    TRAILER SUPPORT CLAMP	Bolt type, alloy, 31.8mm
    FORK	SR Suntour XCT-JR, coil spring w / preload adjust, 50mm of travel, 1-1 / 8 "steerer, quick-release, V-brake and post mount disc compatible
    FRAME	Specialized ™ A1 Premium Aluminum, single-butted, unisex frame, replaceable derailleur hanger`,
    price: '490',
    catid: 1,
  },
  {
    id: 7,
    image: require('../assets/data/kids/Specialized_Hotwalk_12.jpg'),
    title: `Specialized Hotwalk 12''`,
    desc: `Frame	Specialized A1 Premium Aluminum, 1 "threaded headset
    Handlebars	Steel, 400mm width, 20mm rise, 8-degree backsweep
    Handles / handlebar tape	Specialized , slip-on
    Seat post	Steel, 22.2mm
    Seat clamp	Bolt-type, 25.4mm
    Lmpeller size	12 "
    Rims	12 "alloy, 16mm depth, 21mm internal width
    Front hub / rear hub	Bolt-on, 95mm spacing, loose ball bearings, 16h / Bolt-on, 95mm spacing, loose ball bearings, 16h
    Spokes	Stainless, 14g
    Front / rear tires	Rhythm Lite Airless, 12x2.3 "`,
    price: '350',
    catid: 1,
  },
  {
    id: 9,
    image: require('../assets/data/fat/KONA_Woo_26.jpg'),
    title: `KONA Woo 26''`,
    desc: `KONA Woo 26'' Fat bike bike size S gloss prism purple-blue 2021  

    Specification	Description
    Frame	Kona Fat 6061 Aluminum Butted
    Fork	Kona Wo Fat Carbon Disc
    Headset	FSA No.57B
    Stem	Kona XC/BC 35
    Handlebar	Kona XC/BC 35
    Grips / Handlebar tape	Kona Key Grip
    Shifter	SRAM SX-Eagle
    Brake lever	Shimano MT410
    Rear Derailleur	SRAM SX-Eagle, 12-speed
    Bottom Bracket	SRAM DUB PF121
    Crank	SRAM NX-Eagle Dub Fat, X-sync Eagle, 30t
    Cassette	SRAM SX-Eagle, 11-50t
    Chain	SRAM SX-Eagle
    Brakes	Shimano MT410 Hydraulic
    Brake discs	Shimano RT56 180mm / Shimano RT56 160mm
    Saddle	Kona Fat
    Seatpost	Kona XC/BC 31.6mm
    Seat Clamp	Kona QR
    Wheels size	26''
    Tyres	Sun Ringle Mulefut 80 Tubeless Ready
    Front Hub / Rear Hub	Formula 150x15mm / Formula 197x12mm
    Spokes	Stainless Black 14g
    Front Tyre / Rear Tyre	Vee Tire Snow Avalanche TR 26x4.8" / Vee Tire Snowshoe XL TR 26x4.8"`,
    price: '655',
    catid: 2,
  },
  {
    id: 10,
    image: require('../assets/data/fat/Salsa_Beargrease_carbon_Deore_27.5.jpg'),
    title: `Salsa Beargrease 27.5''`,
    desc: `Frame	Beargrease Carbon V2
    Fork	Bearpaw
    Headset	Cane Creek 40
    Stem	Salsa Guide Trail 35.0
    Handlebar	Salsa Guide 35.0
    Grips / Handlebar tape	Salsa File Tread Lock-On
    Shifter	Shimano Deore
    Brake lever	Tektro HD-M275/HD-M276
    Rear Derailleur	Shimano Deore, 11-speed
    Crank	Samox, 24mm spindle, Direct Mount, 30t
    Cassette	Shimano Deore, 10-51t
    Chain	KMC X-11 EPT Anti-Rust
    Brakes	Tektro HD-M275/HD-M276
    Brake discs	160mm
    Saddle	WTB Volt Steel Medium
    Seatpost	Salsa Guide
    Wheels size	27.5''
    Front Wheel / Rear Wheel	Sun Ringle SRC 15x150mm, Mulefüt SL 80mm / Sun Ringle SRC 12x197mm, Mulefüt SL 80mm
    Front Tyre / Rear Tyre	45NRTH Vanhelga, 27.5x4.0, 120tpi, tubeless ready
    Weight	ca. 13.7 kg`,
    price: '599',
    catid: 2,
  },
  {
    id: 11,
    image: require('../assets/data/fat/Salsa_Mukluk_aluminium_Deore_26.jpg'),
    title: `Salsa Mukluk Deore 26''`,
    desc: `Frame	Mukluk Aluminium
    Fork	Bearpaw
    Headset	FSA Orbit
    Stem	Salsa Guide Trail 35.0
    Handlebar	Salsa Guide Deluxe 35.0
    Grips / Handlebar tape	Salsa File Tread Lock-On
    Shifter	Shimano Deore
    Brake lever	Tektro HD-M275/HD-M276
    Rear Derailleur	Shimano Deore, 11-speed
    Crank	Samox two-piece, Direct Mount, 30t
    Cassette	Shiamno Deore 11-51t
    Chain	KMC X-11 EPT Anti-Rust
    Brakes	Tektro HD-M275/HD-M276
    Brake discs	160mm
    Saddle	WTB Volt Steel Medium
    Seatpost	Salsa Guide
    Wheels size	26''
    Front Wheel / Rear Wheel	Sun Ringle SRC 15x150mm, Mulefüt SL 80mm / Sun Ringle SRC 12x197mm, Mulefüt SL 80mm
    Front Tyre / Rear Tyre	45NRTH Dillinger 5, 26x4.6'', 60tpi, tan wall, custom-studdable, tubeless ready
    Weight	ca. 15.0 kg`,
    price: '500',
    catid: 2,
  },
  {
    id: 12,
    image: require('../assets/data/fat/Trek_Farley_5_27.5.jpg'),
    title: `Trek Farley 5 27.5 MTB`,
    desc: `Frame	Alpha Platinum Aluminum, conical head tube, internal cable routing for gears and vario seat post, luggage rack eyelets, horizontally adjustable dropouts (197 x 12 mm)
    Fork	Bontrager Haru, tapered aluminum steerer, OCLV carbon fork blades, 490 mm axle to crown, 15 x 150 mm thru axle
    Stem	Bontrager Elite, 31.8 mm clamp diameter, Blendr compatible, 7 degrees, 60 mm length (S) / Bontrager Elite, 31.8 mm clamp diameter, Blendr compatible, 7 degrees, 70 mm length (M) / Bontrager Elite, 31.8 mm length (M) , 8 mm clamp diameter, Blendr compatible, 7 degrees, 80 mm length (L) / Bontrager Elite, 31.8 mm clamp diameter, Blendr compatible, 7 degrees, 90 mm length (XL)
    Handlebars	Bontrager made of aluminum, 31.8 mm, 15 mm rise, 750 mm width
    Handles / handlebar tape	Bontrager XR Trail Comp, nylon clamp
    Gear lever	Shimano Deore M4100, 10-speed
    Rear derailleur	Shimano Deore M5120, long cage
    Bottom bracket	Race Face, 121 mm, Pressfit
    Crankset	Race Face Ride, steel chainring (28 Z.), 170mm (S) / Race Face Ride, steel chainring (28 Z.), 175 mm (M-XL)
    cassette	Shimano Deore M4100, 11-46 T, 10-speed
    Chain	KMC X10, for 10-speed cassette
    Brakes	Hydraulic disc brake SRAM Level
    Brake discs	Avid G2 Cleansweep, 160 mm, 6-hole disc holder
    saddle	Bontrager Arvada, steel rails, 138 mm wide
    Seat post	TranzX JD-YSP18, 100 mm stroke, internal cable routing, 31.6 mm, 361 mm length (S) / TranzX JD-YSP18, 120 mm stroke, internal cable routing, 31.6 mm, 403 mm length (M) / TranzX JD -YSP18, 130 mm stroke, internal routing, 31.6 mm, 425 mm length (L-XL)
    Impeller size	27.5 ''
    Rims	SUNringlé Mulefüt 80 SL, 32-hole, recess
    Front hub / rear hub	Bontrager made of aluminum, sealed bearing, 6-bolt disc mount, 150 x 15 mm thru-axle / Bontrager, aluminum, sealed bearing, 6-hole disc mount, Rapid Drive (108 T.), 10-speed freehub by Shimano, 197 x 12 mm thru axle
    Spokes	15 g, stainless steel
    Front / rear tires	Bontrager Gnarwhal Team Issue, Tubeless Ready, Inner Strength side protection, retrofittable with spikes, aramid bead core, 120 TPI, 27.5 x 4.50 "
    Pedals	VP-536, nylon platform
    Weight	M - 14.66 kg
    Max. Permissible total weight	136 kg`,
    price: '435',
    catid: 2,
  },
  {
    id: 13,
    image: require('../assets/data/fat/Trek_Farley_9.6_27.5.jpg'),
    title: `Trek Farley 9.6 27.5 MTB`,
    desc: `Frame	OCLV Mountain Carbon, conical head tube, internal cable routing for gears and vario seat post, horizontally sliding dropouts for thru axles (197 x 12 mm)
    Fork	Bontrager Haru, tapered aluminum steerer, OCLV carbon fork blades, 490 mm axle to crown, 15 x 150 mm thru axle
    Stem	Bontrager Elite, 31.8 mm clamp diameter, Blendr compatible, 7 degrees, 60 mm length (S) / Bontrager Elite, 31.8 mm clamp diameter, Blendr compatible, 7 degrees, 70 mm length (M) / Bontrager Elite, 31.8 mm length (M) , 8 mm clamp diameter, Blendr compatible, 7 degrees, 80 mm length (L) / Bontrager Elite, 31.8 mm clamp diameter, Blendr compatible, 7 degrees, 90 mm length (XL)
    Handlebars	Bontrager Rhythm Comp, aluminum, 31.8 mm, 15 mm rise, 750 mm width
    Handles / handlebar tape	Bontrager XR Trail Elite, aluminum clamp
    Gear lever	SRAM GX Eagle, 12-speed
    Rear derailleur	SRAM GX Eagle
    Bottom bracket	SRAM DUB, 121 mm, press fit
    Crankset	SRAM GX Eagle, DUB, FAT 5 aluminum chainring (30 Z.), 170 mm crank arm length (S) / SRAM GX Eagle, DUB, FAT 5 aluminum chain ring (30 Z.), 175 mm crank arm length (M-XL)
    Cassette	SRAM DUB, 121 mm, press fit
    Chain	SRAM GX Eagle, 12-speed
    Brakes	Hydraulic disc brake SRAM Level TL
    Brake discs	SRAM Centerline, 6-hole, rounded edge, 160 mm
    Saddle	Bontrager Arvada, rails made of CrMo tubing, 138 mm wide
    Seat post	TranzX JD-YSP18, 100 mm stroke, internal cable routing, 31.6 mm, 361 mm length (S) / TranzX JD-YSP18, 120 mm stroke, internal cable routing, 31.6 mm, 403 mm length (M) / TranzX JD -YSP18, 130 mm stroke, internal routing, 31.6 mm, 425 mm length (L-XL)
    Impeller size	27.5 ''
    Rims	SUNringlé Mulefüt 80 SL, 32-hole, recess
    Front hub / rear hub	Bontrager, aluminum, sealed bearing, 6-hole disc mount, 15 x 150 mm thru-axle / Bontrager, aluminum, sealed bearing, 6-hole disc mount, Rapid Drive (108T), SRAM XD freehub body, 197 x 12 mm -Through axis
    Spokes	15 g, stainless steel
    Front / rear tires	Bontrager Barbegazi Team Issue, Tubeless Ready, Inner Strength Flank Protection, Aramid Bead Core, 120 TPI, 27.5 x 4.50
    Pedals	VP-536, nylon platform
    Weight	M - 13.20 kg
    Max. Permissible total weight	136 kg`,
    price: '30',
    catid: 2,
  },
  {
    id: 17,
    image: require('../assets/data/pedelecs/Bergamont_E-Ville_Edition_28.jpg'),
    title: `Bergamont E-Ville 28''`,
    desc: `Frame	28'', lite AL-6061 Rohrsatz, PowerTube Akku
    Fork	Suntour Mobie 25, RLR DS, 100 mm, 15x100 mm Achse
    Headset	Acros A-Headset, semi-integriert, 1.5", mit integrierter Kabelführung
    Stem	BGM Pro, einstellbar, 17°-25°
    Handlebar	BGM Pro, Riser Lenker, Kröpfung: 15°, Höhe: 5 mm
    Grips / Handlebar tape	BGM Comfort, Ergo, Double Density, Schraubgriffe
    Shifter	Shimano Deore, SL-M4100, 1x10-fach, Rapidfire Plus-Schalthebel
    Brake lever	Shimano BL-MT200, hydraulische Scheibenbremse
    Rear Derailleur	Shimano Deore, RD-M4120, Shadow
    Bottom Bracket	Bosch, Isis
    Crank	FSA CK-745, 38t
    Cassette	Shimano Deore, CS-M4100, 11-42t
    Chain	KMC e10S
    Brakes	Shimano BR-MT200, hydraulische Scheibenbremse
    Brake discs	SM-RT10 Rotor: 180/160 mm
    Saddle	Selle Royal Lookin BGM Basic Athletic
    Seatpost	BGM Pro
    Motor	Bosch Drive Unit Performance Line CX Cruise, Gen. 4, 250 W, 36 V, max. 25 km/h
    Battery	Bosch 36 V Li Ion, 500 Wh, PowerTube
    Display	Bosch Purion
    Wheels size	28''
    Tyres	Ryde Rival 23, Disc, geöst
    Front Hub / Rear Hub	Shimano HB-MT400, Centerlock, Disc, 15x110 mm Achse / Shimano FH-MT200-B, Centerlock, Disc, Schnellspanner
    Spokes	Sapim Leader
    Front Tyre / Rear Tyre	Schwalbe Marathon E-Plus, Smart Dual Guard, Reflex-Streifen, 55-622
    Tubes / Tyre Sealant	Schwalbe AV19
    Lighting	Herrmans H-Black MR4, LED, E-Bike Version (Frontlicht) / Herrmans H-Trace E, LED (Rücklicht)
    Rack	Racktime E-Ville Carrier
    Fenders	BGM special alloy, mit Edelstahlstreben
    Pedals	Trekking Aluminium
    Kickstand	Syncros
    Weight	ca. 26.9 kg
    Total weight allowed	160 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '225',
    catid: 3,
  },
  {
    id: 18,
    image: require('../assets/data/pedelecs/Specialized_Turbo_Como_3.0_Low-Entry_700C_28.jpg'),
    title: `Turbo Como 3.0 Low-Entry 700C`,
    desc: `Frame	Turbo Aluminum, low-entry frame, bottom bracket motor mount, Ground Control Geometry, fully integrated & lockable down tube battery, internal cable routing, fender / rack mounts, Smooth Welds, 141mm QR Dropout
    Fork	SR Suntour MOBIE30, steel steerer, 9mm open dropout, 50mm of travel
    Stem	Specialized Flowset, 3D-forged alloy, 20-degree rise, 31.8mm clamp
    Handlebars	Specialized , alloy, 30-degree backsweep, 26mm rise, 680mm width, 31.8mm
    Grips / handlebar tape	Body Geometry Women's Contour
    Gear lever	Shimano Alivio, 9-speed w / Optical Gear Display
    Rear derailleur	Shimano Alivio, Shadow Design, SGS cage, 9-speed
    Crankset	Custom alloy forged crankarms, chainring: 40T, custom alloy
    cassette	Shimano CS-HG200, 9-speed, 11-36t
    Chain	KMC e9T.NPNP, e-bike chain, 9-speed w / Missing Link ™
    Brakes	Shimano BR-MT200, hydraulic disc
    Brake discs	180mm (front) / 160mm (rear)
    Saddle	Body Geometry Comfort Gel, 200mm w / SWAT ™ Mount
    Seat post	Specialized , 6061 alloy, 2-bolt clamp, 0mm setback, 30.9mm
    Seat clamp	Alloy, bolt-type, 34.9mm
    Engine	Specialized 1.2 E, custom Rx Street-tuned motor, 250W nominal
    Battery pack	Specialized U1-460, On / Off button, state of charge display, 460Wh
    Display	Turbo Connect Display (TCD)
    charger	Custom Specialized 42V2A Charger w / Rosenberger plug, AC power cord included
    Wiring harness	Custom Specialized harness
    Impeller size	28 "
    Rims	700C disc, double-wall alloy, pinned, no eyelets
    VR hub / HR hub	Shimano HB-MT200, 9x100mm QR, Center Lock ™ disc, 32h / Shimano FH-MT200, 9x141mm QR, Center Lock ™ disc, 32h
    Spokes	DT Swiss Industry
    Hoses / sealing milk	Presta, 40mm valve
    lighting	Herrmans MR4 e-Fahrrad, stainless bracket for suspension fork, 6V (front light) / Herrmans H-Trace Mini e-Fahrrad, 220-degree visibility, fender-mount (Taillight)
    Luggage rack	Turbo Rack, Closed Platform, direct fender mount, Racktime-compatible
    Fenders	Specialized DRYTECH fenders, 52mm width, stainless steel fenderstays
    Pedals	Specialized Commuter w / grip tape & reflectors
    Stand	Specialized kickstand, 40mm mount
    Others	Bell: Simple bell`,
    price: '299',
    catid: 3,
  },
  {
    id: 19,
    image: require('../assets/data/pedelecs/Specialized_Turbo_Vado_3.0_28.jpg'),
    title: `Specialized Turbo Vado 3.0 28''`,
    desc: `Frame	Turbo Aluminum, bottom bracket motor mount, Fitness / Transportation Geometry, fully integrated & lockable down tube battery, internal cable routing, fender / rack mounts, Smooth Welds
    Fork	SR SunTour MOBIE30, custom crown, steel steerer, 9mm open dropout, 50mm of travel
    Stem	Specialized Flowset, 3D-forged alloy, 20-degree rise, 31.8mm clamp
    Handlebars	Specialized , alloy, 9-degree backsweep, 15mm rise, 31.8mm
    Grips / handlebar tape	Specialized Body Geometry Targa, lock-on
    Gear lever	Shimano Alivio, 9-speed w / Optical Gear Display
    Rear derailleur	Shimano Alivio, Shadow Design, SGS cage, 9-speed
    Crankset	Custom alloy forged crankarms, chainring: 40T, custom alloy
    cassette	Shimano CS-HG200, 9-speed, 11-36t
    Chain	KMC e9T.NPNP, e-bike chain, 9-speed w / Missing Link ™
    Brakes	Tektro HD-T275, hydraulic disc
    Brake discs	180mm (front) / 160mm (rear)
    Saddle	Specialized Canopy Sport, steel rails, 155mm
    Seat post	Specialized 2-bolt head, forged alloy, 8mm offset, micro-adjust, 30.9mm
    Seat clamp	Alloy, bolt-type, 34.9mm
    Engine	Specialized 1.2 E, custom Rx Street-tuned motor, 250W nominal
    Battery pack	Specialized U1-460, On / Off button, state of charge display, 460Wh
    Charger	Custom Specialized 42V2A Charger w / Rosenberger plug, AC power cord included
    Wiring harness	Custom Specialized harness
    Impeller size	28 "
    Rims	700C disc, double-wall alloy, pinned, no eyelets
    VR hub / HR hub	Specialized 6-bolt disc, ball bearings, 9x100mm spacing, 32h / Specialized , sealed cartridge bearings, 12x148mm thru-axle, 32h
    Spokes	DT Swiss Industry
    Front / rear tires	Trigger Sport Reflect, 700x47mm
    Hoses / sealing milk	Presta, 40mm valve
    Lighting	Herrmans MR4 e-Fahrrad, stainless bracket for suspension fork, 6V (front light) / Herrmans H-Trace Mini e-Fahrrad, 220-degree visibility, fender-mount (Taillight)
    Luggage rack	Turbo Rack, Closed Platform, direct fender mount, Racktime-compatible
    Fenders	DRYTECH fenders, 52mm width, stainless steel fenderstays
    Pedals	Specialized Commuter w / grip tape & reflectors
    Stand	Specialized kickstand, 40mm mount
    Others	Simple bell bell`,
    price: '300',
    catid: 3,
  },
  {
    id: 20,
    image: require('../assets/data/pedelecs/Specialized_Turbo_Vado_SL_4.0_EQ_28.jpg'),
    title: `Turbo Vado SL 4.0 EQ 28''`,
    desc: `Frame	E5 Aluminum, Fitness / Transportation Geometry, bottom bracket motor mount, fully integrated down tube battery, internal cable routing, fender / rack mounts, Smooth Welds, reflective graphics
    Fork	Rigid full aluminum disc, Boost ™ 12x110mm, front-rack compatible
    Stem	Specialized Stealth Stem, alloy, 14 deg, 31.8mm, integrated TCD-W mount
    Handlebars	Stout Mini Rise, alloy, 9-degree backsweep, 15mm rise, 31.8mm
    Grips / handlebar tape	Specialized Body Geometry Contour, lock-on
    Gear lever	Shimano Deore, RapidFire Plus, 10-speed w / Optical Gear Display
    Brake lever	Tektro HD-R290
    Rear derailleur	Shimano Deore, Shadow Plus, GS cage, 10-speed
    Crankset	Practice, Forged alloy M30, custom offset, 44t, 104BCD
    Cassette	Shimano Deore, 10spd, 11-42t
    Chain	KMC e10S, 10-speed w / Missing Link ™
    Brakes	Tektro HD-R290, hydraulic disc
    Brake discs	160mm
    Saddle	Bridge Sport, Steel rails, 155mm
    Seat post	Specialized , alloy, dual-bolt, 25mm offset, 27.2mm
    Seat clamp	Integrated w / frame
    Engine	Specialized SL 1.1, custom lightweight motor
    Battery pack	Specialized SL1-320, fully integrated, 320Wh
    Display	Specialized TCU, 10-LED State of charge, 3-LED Ride Mode display, ANT + / Bluetooth®
    Charger	Custom charger, 48V system w / SL system charger plug
    Wiring harness	Custom Specialized wiring harness w / chargeport
    Impeller size	28 ''
    Rims	700C disc, 22mm rim depth, 21mm internal width
    VR hub / HR hub	Specialized alloy front hub disc, sealed cartridge bearings, 12x110mm, Center Lock ™, 24h / Specialized alloy rear hub disc, Center Lock ™, sealed cartridge bearings, 12x148mm, 28h
    Spokes	DT Swiss Industry
    Front / rear tires	Nimbus II Sport Reflect, 700x38mm
    Hoses / sealing milk	Presta, 40mm valve
    Lighting	Lezyne Ebike Hecto STVZO E65, 210Lumens, 12V (front light) / Lezyne Ebike Rear Fender STVZO, 11Lumens, 12V (rear light)
    Luggage rack	Turbo SL Rack, Closed Platform, direct fender mount, Racktime-compatible, 15kg
    Fenders	Specialized DryTech, Aluminum
    Pedals	Specialized Commuter w / grip tape & reflectors
    Stand	Specialized kickstand, 40mm mount
    Others	Simple bell bell; Specialized Stealth Stem, Light Insert (SWAT)`,
    price: '350',
    catid: 3,
  },
  {
    id: 21,
    image: require('../assets/data/pedelecs/Specialized_Turbo_Vado_SL_5.0_EQ_28.jpg'),
    title: `Turbo Vado SL 5.0 EQ 28''`,
    desc: `Frame	E5 Aluminum, Fitness / Transportation Geometry, bottom bracket motor mount, fully integrated down tube battery, internal cable routing, fender / rack mounts, Smooth Welds, reflective graphics
    Fork	Future Shock 1.5, Boost ™ 12x110mm thru-axle, flat mount disc, carbon fork
    Stem	Specialized Stealth Stem, alloy, 14 deg, 31.8mm, integrated TCD-W mount
    Handlebars	Stout Mini Rise, alloy, 9-degree backsweep, 15mm rise, 31.8mm
    Grips / handlebar tape	Specialized Body Geometry Contour, lock-on
    Gear lever	Shimano SLX, Rapidfire Plus, 12-speed, w / o OGD
    Brake lever	Tektro HD-R510
    Rear derailleur	Shimano XT, Shadow Plus, SGS Cage, 12-speed
    Crankset	Practice, Forged alloy M30, custom offset, 44t, 104BCD
    cassette	Shimano SLX, Hyperglide +, 12-speed, 10-45t
    Chain	Shimano SLX, 12-speed, w / Quick Link
    Brakes	Tektro HD-R510, hydraulic disc
    Brake discs	160mm
    Saddle	Bridge Sport, hollow cr-mo rails, 155mm
    Seat post	Specialized , alloy, single bolt, 21mm offset, 27.2mm
    Seat clamp	Integrated w / frame
    Engine	Specialized SL 1.1, custom lightweight motor
    Battery pack	Specialized SL1-320, fully integrated, 320Wh
    Display	Specialized TCU, 10-LED State of charge, 3-LED Ride Mode display, ANT + / Bluetooth®, incl.TCD computer
    Charger	Custom charger, 48V system w / SL system charger plug
    Wiring harness	Custom Specialized wiring harness w / chargeport
    Impeller size	28 ''
    Rims	DT Swiss R500 disc, 22mm internal width, 23mm internal depth
    VR hub / HR hub	Specialized alloy front hub disc, sealed cartridge bearings, 12x110mm, Center Lock ™, 24h / Specialized alloy rear hub disc, Center Lock ™, sealed cartridge bearings, 12x148mm, 28h
    Spokes	DT Swiss Industry
    Front / rear tires	Nimbus II Sport Reflect, 700x38mm
    Hoses / sealing milk	Presta, 40mm valve
    Lighting	Lezyne Ebike Power STVZO E115, 310Lumens, 12V (front light) / Lezyne Ebike Rear Fender STVZO, 11Lumens, 12V (rear light)
    Luggage rack	Turbo SL Rack, Closed Platform, direct fender mount, Racktime-compatible, 15kg
    Fenders	Specialized DryTech, Aluminum
    Pedals	Specialized Commuter w / grip tape & reflectors
    Stand	Specialized kickstand, 40mm mount
    Others	Simple bell bell; Specialized Stealth Stem, Light Insert (SWAT)`,
    price: '459',
    catid: 3,
  },
  {
    id: 22,
    image: require('../assets/data/pedelecs/Trek_Allant+_8_27.5_2021.jpg'),
    title: `Trek Allant+ 8 27.5 2021`,
    desc: `Frame	High-performance hydroformed e-bike frame with integrated battery (Range Boost compatible) and motor armor, internal cable routing, post mount disc brake mount, dropouts for 5 mm quick release axle
    Fork	Allant +, rigid aluminum fork, 1 1/8 "steel fork shaft, 100 x 15 mm thru-axle, post-mount disc brake mount
    Stem	Bontrager Elite Blendr, 7 degree rise, 31.8 mm clamp diameter, with bracket for Supernova M99 headlight
    Handlebars	Aluminum comfort
    Handles / handlebar tape	Bontrager Satellite Elite, aluminum clamp
    Gear lever	Shimano Deore Rapid-Fire Plus, 10-speed
    Rear derailleur	Shimano Deore M6000, medium-length cage, max. 42 teeth on the largest sprocket
    Crankset	ProWheel, 40 Z., Narrow / Wide, steel, with aluminum chain guard
    Cassette	Shimano HG500, 11-42 T., 10-speed
    Chain	KMC E10S
    Brakes	Hydraulic disc brake Shimano MT200, Post Mount, 180 mm disc diameter
    Saddle	Bontrager Commuter Comp, 165 mm wide
    Seat post	Bontrager Approved, 31.6mm, 8mm offset
    Engine	Bosch Performance Line CX, 75 Nm, 25 km / h
    Battery pack	Bosch PowerTube, 625 Wh
    Display	Bosch Kiox with theft protection
    Impeller size	27.5 "
    Front / rear tires	Bontrager E6 Hard-Case Lite, reflective, wire bead core, 60 TPI, 27.5 x 2.4
    Pedals	Bontrager Satellite City
    Weight	L- 20.8 kg`,
    price: '545',
    catid: 3,
  },
  {
    id: 23,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_8_28.jpg'),
    title: `Urance 8 28'' Gravel`,
    desc: `Frame	28'', ultra lite AL-6061 Rohrsatz
    Fork	Grandurance Carbon II, Carbon steerer, 12x100 mm Achse, Schutzblechaufnahme
    Headset	BGM F13-E-476, A-Headset, semi-integriert, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano GRX
    Rear Derailleur	Shimano GRX, RD-RX812, Shadow Plus
    Bottom Bracket	Shimano Hollowtech II, BSA
    Crank	Shimano GRX, FC-RX600, 40t
    Cassette	Shimano SLX, CS-M7000, 11-42t
    Chain	KMC X11
    Brakes	Shimano GRX, BR-RX400, hydraulische Scheibenbremse
    Brake discs	SM-RT64 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Syncros Capital 2.0, Disc
    Front Hub / Rear Hub	BGM Allroad, Centerlock, Disc, 12x100 mm Achse / BGM Allroad, Centerlock, Disc, 12x142 mm Achse
    Spokes	double butted, Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Faltreifen, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Weight	ca. 9.8 kg
    Total weight allowed	115 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '255',
    catid: 4,
  },
  {
    id: 24,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_Elite_28.jpg'),
    title: `Elite 28'' Gravel`,
    desc: `Frame	28'', ultra lite HSC Carbon
    Fork	Grandurance Carbon, Carbon steerer, 12x100 mm Achse, Schutzblechaufnahme
    Headset	BGM PT1860-476, A-Headset, integrated, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano GRX
    Rear Derailleur	Shimano GRX, RD-RX812, Shadow Plus
    Bottom Bracket	Shimano BB-RS500, press-fit
    Crank	Shimano GRX, FC-RX600, 40t
    Cassette	Shimano SLX, CS-M7000, 11-42t
    Chain	KMC X11
    Brakes	Shimano GRX, BR-RX400, hydraulische Scheibenbremse
    Brake discs	SM-RT64 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Syncros Capital 2.0, Disc
    Front Hub / Rear Hub	BGM Allroad, Centerlock, Disc, 12x100 mm Achse / BGM Allroad, Centerlock, Disc, 12x142 mm Achse
    Spokes	double butted, Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Faltreifen, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Weight	ca. 9.2 kg
    Total weight allowed	110 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '450',
    catid: 4,
  },
  {
    id: 25,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_Expert_28.jpg'),
    title: `Expert 28'' Gravel`,
    desc: `Frame	28'', ultra lite HSC Carbon
    Fork	Grandurance Carbon, Carbon steerer, 12x100 mm Achse, Schutzblechaufnahme
    Headset	BGM PT1860-476, A-Headset, integrated, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano GRX
    Rear Derailleur	Shimano GRX, RD-RX400, Shadow Plus
    Front Derailleur	Shimano GRX, FD-RX400
    Bottom Bracket	Shimano BB-RS500, press-fit
    Crank	Shimano GRX, FC-RX600, 46/30t
    Cassette	Shimano CS-HG50, 11-36t
    Chain	KMC X10
    Brakes	Shimano GRX, BR-RX400, hydraulische Scheibenbremse
    Brake discs	SM-RT64 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Shining DB-T260, Disc
    Front Hub / Rear Hub	BGM Allroad, Centerlock, Disc, 12x100 mm Achse / BGM Allroad, Centerlock, Disc, 12x142 mm Achse
    Spokes	Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Faltreifen, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Weight	ca. 9.6 kg
    Total weight allowed	110 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '500',
    catid: 4,
  },
  {
    id: 26,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_RD_3_28.jpg'),
    title: `RD 3 28'' Gravel`,
    desc: `Frame	28'', ultra lite AL-6061 Rohrsatz
    Fork	Grandurance Aluminium, Schnellspanner, Schutzblechaufnahme
    Headset	BGM F13-C, A-Headset, semi-integriert, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano Claris
    Rear Derailleur	Shimano Claris, RD-R2000, long cage
    Front Derailleur	Shimano Claris, FD-R2000
    Bottom Bracket	Shimano BB-UN300, BSA
    Crank	Shimano, FC-RS200, 50/34t
    Cassette	Shimano CS-HG31-8, 12-34t
    Chain	KMC X8
    Brakes	Shimano BR-R317, mechanische Scheibenbremse
    Brake discs	SM-RT54 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Shining DB-T260, Disc
    Front Hub / Rear Hub	Shimano Nabendynamo, DH-3D72, Centerlock, Disc, Schnellspanner / BGM Pro, Centerlock, Disc, Schnellspanner
    Spokes	Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Lighting	Herrmans H-Black MR4, LED, Standlicht (Frontlicht) / Herrmans H-Trace Mini, LED, Standlicht (Rücklicht)
    Rack	Racktime / BGM Allroad Carrier, SnapIt
    Fenders	BGM Allroad Fender, 40 mm
    Weight	ca. 13.1 kg
    Total weight allowed	115 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '450',
    catid: 4,
  },
  {
    id: 27,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_RD_5_28.jpg'),
    title: `Urance RD 5 28''`,
    desc: `Frame	28'', ultra lite AL-6061 Rohrsatz
    Fork	Grandurance Aluminium, Schnellspanner, Schutzblechaufnahme
    Headset	BGM F13-C, A-Headset, semi-integriert, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano Tiagra
    Rear Derailleur	Shimano Tiagra, RD-4700, long cage
    Front Derailleur	Shimano Tiagra, FD-4700
    Bottom Bracket	Shimano Hollowtech II, BSA
    Crank	Shimano Tiagra, FC-4700, 50/34t
    Cassette	Shimano CS-HG500, 11-34t
    Chain	KMC X10
    Brakes	Shimano BR-RS785, hydraulische Scheibenbremse
    Brake discs	SM-RT54 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Shining DB-T260, Disc
    Front Hub / Rear Hub	Shimano Nabendynamo, DH-3D72, Centerlock, Disc, Schnellspanner / BGM Pro, Centerlock, Disc, Schnellspanner
    Spokes	Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Lighting	Herrmans H-Black MR4, LED, Standlicht (Frontlicht) / Herrmans H-Trace Mini, LED, Standlicht (Rücklicht)
    Rack	Racktime / BGM Allroad Carrier, SnapIt
    Fenders	BGM Allroad Fender, 40 mm
    Weight	ca. 12.6 kg
    Total weight allowed	115 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '35',
    catid: 4,
  },
  {
    id: 28,
    image: require('../assets/data/gravel/KONA_Sutra_AL_SE_28.jpg'),
    title: `KONA Sutra AL SE 28"`,
    desc: `Frame	Kona 6061 aluminum butted
    Fork	Kona Project One Two Aluminum Disc
    Stem	Kona Road
    Handlebars	Kona Road
    Handles / handlebar tape	Kona cork tape
    Gear lever	Shimano Bar Con
    Brake lever	Trektro RL340
    Rear derailleur	Shimano Deore SGS
    Derailleur	Shimano Sore
    Bottom bracket	Shimano 68mm
    Crankset	Shimano Sora 30t / 39t / 50t
    cassette	Shimano Alivio 11-34t 9-speed
    Chain	KMC Z9
    Brake caliper	Hayes CX Comp
    Brake discs	Hayes L-Series 160mm
    Saddle	Kona Road
    Seat post	Kona Deluxe Thumb w / Offset 27.2mm
    Seat clamp	Kona clamp
    Impeller size	700
    Rims	Double wall alloy
    Front hub / rear hub	Shimano Deore 100x9mm / Shimano Deore 135x10mm
    Spokes	Stainless Black 14g
    Front / rear tires	Schwalbe Delta Cruiser Plus 700x35c`,
    price: '20',
    catid: 4,
  },
];
const features = [
  {
    id: 26,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_RD_3_28.jpg'),
    title: `RD 3 28'' Gravel`,
    desc: `Frame	28'', ultra lite AL-6061 Rohrsatz
    Fork	Grandurance Aluminium, Schnellspanner, Schutzblechaufnahme
    Headset	BGM F13-C, A-Headset, semi-integriert, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano Claris
    Rear Derailleur	Shimano Claris, RD-R2000, long cage
    Front Derailleur	Shimano Claris, FD-R2000
    Bottom Bracket	Shimano BB-UN300, BSA
    Crank	Shimano, FC-RS200, 50/34t
    Cassette	Shimano CS-HG31-8, 12-34t
    Chain	KMC X8
    Brakes	Shimano BR-R317, mechanische Scheibenbremse
    Brake discs	SM-RT54 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Shining DB-T260, Disc
    Front Hub / Rear Hub	Shimano Nabendynamo, DH-3D72, Centerlock, Disc, Schnellspanner / BGM Pro, Centerlock, Disc, Schnellspanner
    Spokes	Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Lighting	Herrmans H-Black MR4, LED, Standlicht (Frontlicht) / Herrmans H-Trace Mini, LED, Standlicht (Rücklicht)
    Rack	Racktime / BGM Allroad Carrier, SnapIt
    Fenders	BGM Allroad Fender, 40 mm
    Weight	ca. 13.1 kg
    Total weight allowed	115 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '450',
    catid: 4,
  },

  {
    id: 6,
    image: require('../assets/data/kids/Specialized_Hotrock_24.jpg'),
    title: `Specialized Hotrock 24'' MTB`,
    desc: `RIMS	24 ", single wall aluminum, 32h
    FRONT TIRE	Specialized ™ Renegade, 24x2.1 ", 26 TPI, wire bead
    REAR TIRE	Specialized ™ Renegade, 24x2.1 ", 26 TPI, wire bead
    SPOKES	Stainless, 14g
    TUBES	Schrader valve
    FRONT HUB	Alloy, loose ball bearings, bolt type, 32h
    REAR HUB	Alloy, 8-speed freehub, loose ball bearings, bolt type, 32h
    PEDALS	Nylon platform w / reflectors
    BOTTOM BRACKET	BSA, square taper, E-type
    SWITCHER	Shimano Tourney TX, 8-speed
    CHAINRINGS	34T
    CASSETTE	Sunrace, 8-speed, 11-34t
    CRANK	Forged alloy, square taper, 152mm w / chainguard
    ROCKER	Shimano Tourney, Revo, 8-speed
    CHAIN	KMC HV500
    SEAT POST	Alloy, single bolt, 15mm offset, 27.2mm
    BAR	60mm, 5-degree backsweep, 25.4mm
    GRIPS	Specialized ™, slip-on
    SADDLE	Specialized ™ Kids
    STEM	20-degree rise, 25.4mm
    REAR BRAKE	V-brake w / tool-less reach adjust brake lever
    FRONT BRAKE	V-brake, alloy, linear pull, tool-less reach adjust brake lever
    TRAILER SUPPORT CLAMP	Bolt type, alloy, 31.8mm
    FORK	SR Suntour XCT-JR, coil spring w / preload adjust, 50mm of travel, 1-1 / 8 "steerer, quick-release, V-brake and post mount disc compatible
    FRAME	Specialized ™ A1 Premium Aluminum, single-butted, unisex frame, replaceable derailleur hanger`,
    price: '490',
    catid: 1,
  },
  {
    id: 10,
    image: require('../assets/data/fat/Salsa_Beargrease_carbon_Deore_27.5.jpg'),
    title: `Salsa Beargrease 27.5''`,
    desc: `Frame	Beargrease Carbon V2
    Fork	Bearpaw
    Headset	Cane Creek 40
    Stem	Salsa Guide Trail 35.0
    Handlebar	Salsa Guide 35.0
    Grips / Handlebar tape	Salsa File Tread Lock-On
    Shifter	Shimano Deore
    Brake lever	Tektro HD-M275/HD-M276
    Rear Derailleur	Shimano Deore, 11-speed
    Crank	Samox, 24mm spindle, Direct Mount, 30t
    Cassette	Shimano Deore, 10-51t
    Chain	KMC X-11 EPT Anti-Rust
    Brakes	Tektro HD-M275/HD-M276
    Brake discs	160mm
    Saddle	WTB Volt Steel Medium
    Seatpost	Salsa Guide
    Wheels size	27.5''
    Front Wheel / Rear Wheel	Sun Ringle SRC 15x150mm, Mulefüt SL 80mm / Sun Ringle SRC 12x197mm, Mulefüt SL 80mm
    Front Tyre / Rear Tyre	45NRTH Vanhelga, 27.5x4.0, 120tpi, tubeless ready
    Weight	ca. 13.7 kg`,
    price: '599',
    catid: 2,
  },
  {
    id: 18,
    image: require('../assets/data/pedelecs/Specialized_Turbo_Como_3.0_Low-Entry_700C_28.jpg'),
    title: `Turbo Como 3.0 Low-Entry 700C`,
    desc: `Frame	Turbo Aluminum, low-entry frame, bottom bracket motor mount, Ground Control Geometry, fully integrated & lockable down tube battery, internal cable routing, fender / rack mounts, Smooth Welds, 141mm QR Dropout
    Fork	SR Suntour MOBIE30, steel steerer, 9mm open dropout, 50mm of travel
    Stem	Specialized Flowset, 3D-forged alloy, 20-degree rise, 31.8mm clamp
    Handlebars	Specialized , alloy, 30-degree backsweep, 26mm rise, 680mm width, 31.8mm
    Grips / handlebar tape	Body Geometry Women's Contour
    Gear lever	Shimano Alivio, 9-speed w / Optical Gear Display
    Rear derailleur	Shimano Alivio, Shadow Design, SGS cage, 9-speed
    Crankset	Custom alloy forged crankarms, chainring: 40T, custom alloy
    cassette	Shimano CS-HG200, 9-speed, 11-36t
    Chain	KMC e9T.NPNP, e-bike chain, 9-speed w / Missing Link ™
    Brakes	Shimano BR-MT200, hydraulic disc
    Brake discs	180mm (front) / 160mm (rear)
    Saddle	Body Geometry Comfort Gel, 200mm w / SWAT ™ Mount
    Seat post	Specialized , 6061 alloy, 2-bolt clamp, 0mm setback, 30.9mm
    Seat clamp	Alloy, bolt-type, 34.9mm
    Engine	Specialized 1.2 E, custom Rx Street-tuned motor, 250W nominal
    Battery pack	Specialized U1-460, On / Off button, state of charge display, 460Wh
    Display	Turbo Connect Display (TCD)
    charger	Custom Specialized 42V2A Charger w / Rosenberger plug, AC power cord included
    Wiring harness	Custom Specialized harness
    Impeller size	28 "
    Rims	700C disc, double-wall alloy, pinned, no eyelets
    VR hub / HR hub	Shimano HB-MT200, 9x100mm QR, Center Lock ™ disc, 32h / Shimano FH-MT200, 9x141mm QR, Center Lock ™ disc, 32h
    Spokes	DT Swiss Industry
    Hoses / sealing milk	Presta, 40mm valve
    lighting	Herrmans MR4 e-Fahrrad, stainless bracket for suspension fork, 6V (front light) / Herrmans H-Trace Mini e-Fahrrad, 220-degree visibility, fender-mount (Taillight)
    Luggage rack	Turbo Rack, Closed Platform, direct fender mount, Racktime-compatible
    Fenders	Specialized DRYTECH fenders, 52mm width, stainless steel fenderstays
    Pedals	Specialized Commuter w / grip tape & reflectors
    Stand	Specialized kickstand, 40mm mount
    Others	Bell: Simple bell`,
    price: '299',
    catid: 3,
  },
];
const topSaller = [
  {
    id: 27,
    image: require('../assets/data/gravel/Bergamont_G_countour_urance_RD_5_28.jpg'),
    title: `Urance RD 5 28''`,
    desc: `Frame	28'', ultra lite AL-6061 Rohrsatz
    Fork	Grandurance Aluminium, Schnellspanner, Schutzblechaufnahme
    Headset	BGM F13-C, A-Headset, semi-integriert, Tapered
    Stem	Syncros RR2.5, +/- 6°
    Handlebar	Syncros Creston 2.0, Flare: 10°
    Grips / Handlebar tape	BGM Race Grip-Tape, Antislip
    Shift-/brake Lever	Shimano Tiagra
    Rear Derailleur	Shimano Tiagra, RD-4700, long cage
    Front Derailleur	Shimano Tiagra, FD-4700
    Bottom Bracket	Shimano Hollowtech II, BSA
    Crank	Shimano Tiagra, FC-4700, 50/34t
    Cassette	Shimano CS-HG500, 11-34t
    Chain	KMC X10
    Brakes	Shimano BR-RS785, hydraulische Scheibenbremse
    Brake discs	SM-RT54 Rotor: 160/160 mm
    Saddle	Syncros Tofino 2.5
    Seatpost	Syncros 2.5
    Wheels size	28''
    Tyres	Shining DB-T260, Disc
    Front Hub / Rear Hub	Shimano Nabendynamo, DH-3D72, Centerlock, Disc, Schnellspanner / BGM Pro, Centerlock, Disc, Schnellspanner
    Spokes	Edelstahl, schwarz
    Front Tyre / Rear Tyre	Schwalbe G-One Allround, Raceguard, 35-622
    Tubes / Tyre Sealant	Schwalbe SV17A Light
    Lighting	Herrmans H-Black MR4, LED, Standlicht (Frontlicht) / Herrmans H-Trace Mini, LED, Standlicht (Rücklicht)
    Rack	Racktime / BGM Allroad Carrier, SnapIt
    Fenders	BGM Allroad Fender, 40 mm
    Weight	ca. 12.6 kg
    Total weight allowed	115 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '35',
    catid: 4,
  },
  {
    id: 7,
    image: require('../assets/data/kids/Specialized_Hotwalk_12.jpg'),
    title: `Specialized Hotwalk 12''`,
    desc: `Frame	Specialized A1 Premium Aluminum, 1 "threaded headset
    Handlebars	Steel, 400mm width, 20mm rise, 8-degree backsweep
    Handles / handlebar tape	Specialized , slip-on
    Seat post	Steel, 22.2mm
    Seat clamp	Bolt-type, 25.4mm
    Lmpeller size	12 "
    Rims	12 "alloy, 16mm depth, 21mm internal width
    Front hub / rear hub	Bolt-on, 95mm spacing, loose ball bearings, 16h / Bolt-on, 95mm spacing, loose ball bearings, 16h
    Spokes	Stainless, 14g
    Front / rear tires	Rhythm Lite Airless, 12x2.3 "`,
    price: '350',
    catid: 1,
  },
  {
    id: 9,
    image: require('../assets/data/fat/KONA_Woo_26.jpg'),
    title: `KONA Woo 26''`,
    desc: `KONA Woo 26'' Fat bike bike size S gloss prism purple-blue 2021  

    Specification	Description
    Frame	Kona Fat 6061 Aluminum Butted
    Fork	Kona Wo Fat Carbon Disc
    Headset	FSA No.57B
    Stem	Kona XC/BC 35
    Handlebar	Kona XC/BC 35
    Grips / Handlebar tape	Kona Key Grip
    Shifter	SRAM SX-Eagle
    Brake lever	Shimano MT410
    Rear Derailleur	SRAM SX-Eagle, 12-speed
    Bottom Bracket	SRAM DUB PF121
    Crank	SRAM NX-Eagle Dub Fat, X-sync Eagle, 30t
    Cassette	SRAM SX-Eagle, 11-50t
    Chain	SRAM SX-Eagle
    Brakes	Shimano MT410 Hydraulic
    Brake discs	Shimano RT56 180mm / Shimano RT56 160mm
    Saddle	Kona Fat
    Seatpost	Kona XC/BC 31.6mm
    Seat Clamp	Kona QR
    Wheels size	26''
    Tyres	Sun Ringle Mulefut 80 Tubeless Ready
    Front Hub / Rear Hub	Formula 150x15mm / Formula 197x12mm
    Spokes	Stainless Black 14g
    Front Tyre / Rear Tyre	Vee Tire Snow Avalanche TR 26x4.8" / Vee Tire Snowshoe XL TR 26x4.8"`,
    price: '655',
    catid: 2,
  },
  {
    id: 17,
    image: require('../assets/data/pedelecs/Bergamont_E-Ville_Edition_28.jpg'),
    title: `Bergamont E-Ville 28''`,
    desc: `Frame	28'', lite AL-6061 Rohrsatz, PowerTube Akku
    Fork	Suntour Mobie 25, RLR DS, 100 mm, 15x100 mm Achse
    Headset	Acros A-Headset, semi-integriert, 1.5", mit integrierter Kabelführung
    Stem	BGM Pro, einstellbar, 17°-25°
    Handlebar	BGM Pro, Riser Lenker, Kröpfung: 15°, Höhe: 5 mm
    Grips / Handlebar tape	BGM Comfort, Ergo, Double Density, Schraubgriffe
    Shifter	Shimano Deore, SL-M4100, 1x10-fach, Rapidfire Plus-Schalthebel
    Brake lever	Shimano BL-MT200, hydraulische Scheibenbremse
    Rear Derailleur	Shimano Deore, RD-M4120, Shadow
    Bottom Bracket	Bosch, Isis
    Crank	FSA CK-745, 38t
    Cassette	Shimano Deore, CS-M4100, 11-42t
    Chain	KMC e10S
    Brakes	Shimano BR-MT200, hydraulische Scheibenbremse
    Brake discs	SM-RT10 Rotor: 180/160 mm
    Saddle	Selle Royal Lookin BGM Basic Athletic
    Seatpost	BGM Pro
    Motor	Bosch Drive Unit Performance Line CX Cruise, Gen. 4, 250 W, 36 V, max. 25 km/h
    Battery	Bosch 36 V Li Ion, 500 Wh, PowerTube
    Display	Bosch Purion
    Wheels size	28''
    Tyres	Ryde Rival 23, Disc, geöst
    Front Hub / Rear Hub	Shimano HB-MT400, Centerlock, Disc, 15x110 mm Achse / Shimano FH-MT200-B, Centerlock, Disc, Schnellspanner
    Spokes	Sapim Leader
    Front Tyre / Rear Tyre	Schwalbe Marathon E-Plus, Smart Dual Guard, Reflex-Streifen, 55-622
    Tubes / Tyre Sealant	Schwalbe AV19
    Lighting	Herrmans H-Black MR4, LED, E-Bike Version (Frontlicht) / Herrmans H-Trace E, LED (Rücklicht)
    Rack	Racktime E-Ville Carrier
    Fenders	BGM special alloy, mit Edelstahlstreben
    Pedals	Trekking Aluminium
    Kickstand	Syncros
    Weight	ca. 26.9 kg
    Total weight allowed	160 kg (inkl. Fahrrad, Fahrer/in, Bekleidung und Gepäck)`,
    price: '225',
    catid: 3,
  },
];
const cats = [
  {
    id: 1,
    title: 'Bikes for Kids',
    iconw: require('../assets/icons/kidw.png'),
    icon: require('../assets/icons/kid.png'),
  },
  {
    id: 2,
    title: 'Fatbikes',
    iconw: require('../assets/icons/fatw.png'),
    icon: require('../assets/icons/fat.png'),
  },
  {
    id: 3,
    title: 'Pedelecs Bikes',
    iconw: require('../assets/icons/pedelecsw.png'),
    icon: require('../assets/icons/pedelecs.png'),
  },
  {
    id: 4,
    title: 'Gravel Bikes',
    iconw: require('../assets/icons/gravelw.png'),
    icon: require('../assets/icons/gravel.png'),
  },
];

export {cycles, features, topSaller, cats};
