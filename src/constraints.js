const colors = {
  accent: '#98B4C135',
  primary: '#98B4C1',
  secondary: '#1B1B1B',
  tertiary: '#FFE358',
  white: '#FFFFFF',
  gray: '#808080',
  gray2: '#e6e3e3',
  gray3: '#FFFFF3',
  gray4: '#C5CCD6',
  gray5: '#E8E8E8',
  gray6: '#E8E8E860',
};
const sizes = {
  base: 16,
  font: 14,
  padding: 25,
  h1: 26,
  h2: 20,
  h3: 18,
  title: 18,
  header: 16,
};

const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
};

export {colors, sizes, fonts};
